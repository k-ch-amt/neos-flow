<?php

declare(strict_types=1);

namespace Acme\Blog\Controller;

use Acme\Blog\Domain\Model\Blog;
use Flowpack\RestApi\Controller\AbstractRestController;

class BlogRestController extends AbstractRestController
{
    protected static $RESOURCE_ENTITY_CLASS = Blog::class;

    protected $resourceEntityRenderConfiguration = [
        'title',
        'posts',
        'description',
        'blog'
    ];
}
