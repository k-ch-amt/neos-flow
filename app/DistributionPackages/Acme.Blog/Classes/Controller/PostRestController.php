<?php

declare(strict_types=1);

namespace Acme\Blog\Controller;

use Acme\Blog\Domain\Model\Post;
use Flowpack\RestApi\Controller\AbstractRestController;

class PostRestController extends AbstractRestController
{
    protected static $RESOURCE_ENTITY_CLASS = Post::class;

    protected $resourceEntityRenderConfiguration = [
        'uuid',
        'title',
        'author',
        'date',
        'comments',
        'blog',
        'tags',
        'content',
        'subject'
    ];
}
