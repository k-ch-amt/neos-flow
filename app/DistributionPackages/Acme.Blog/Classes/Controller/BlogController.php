<?php

declare(strict_types=1);

namespace Acme\Blog\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use Acme\Blog\Domain\Model\Blog;

class BlogController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Acme\Blog\Domain\Repository\BlogRepository
     */
    protected $blogRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('blogs', $this->blogRepository->findAll());
    }

    /**
     * @param \Acme\Blog\Domain\Model\Blog $blog
     * @return void
     */
    public function showAction(Blog $blog)
    {
        $this->view->assign('blog', $blog);
    }

    /**
     * @return void
     */
    public function newAction()
    {
    }

    /**
     * @param \Acme\Blog\Domain\Model\Blog $newBlog|null
     * @return void
     */
    public function createAction(?Blog $newBlog = null)
    {
        $this->blogRepository->add($newBlog);
        $this->addFlashMessage('Created a new blog.');
        $this->redirectToUri('/blog/index');
    }

    /**
     * @param \Acme\Blog\Domain\Model\Blog $blog
     * @return void
     */
    public function editAction(Blog $blog)
    {
        $this->view->assign('blog', $blog);
    }

    /**
     * @param \Acme\Blog\Domain\Model\Blog $blog
     * @return void
     */
    public function updateAction(Blog $blog)
    {
        $this->blogRepository->update($blog);
        $this->addFlashMessage('Updated the blog.');

        $this->redirectToUri('/blog/index');
    }

    /**
     * @param \Acme\Blog\Domain\Model\Blog $blog
     * @return void
     */
    public function deleteAction(Blog $blog)
    {
        $this->blogRepository->remove($blog);
        $this->addFlashMessage('Deleted a blog.');
        $this->redirectToUri('/blog/index');
    }
}
