<?php

declare(strict_types=1);

namespace Acme\Blog\Domain\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Blog {

    /**
     * @Flow\Validate(type="NotEmpty")
     * @Flow\Validate(type="StringLength", options={ "minimum"=3, "maximum"=80 })
     * @ORM\Column(length=80)
     * @var string
     */
    protected $title;

    /**
     * @Flow\Validate(type="StringLength", options={ "maximum"=150 })
     * @ORM\Column(length=150)
     * @var string
     */
    protected $description = '';

    /**
     * @ORM\OneToMany(mappedBy="blog", targetEntity="Acme\Blog\Domain\Model\Post")
     * @ORM\OrderBy({"date" = "DESC"})
     * @var Collection<Post>
     */
    protected $posts;

    public function __construct(string $title)
    {
        $this->posts = new ArrayCollection();
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): void
    {
        $this->posts->add($post);
    }

    public function removePost(Post $post): void
    {
        $this->posts->removeElement($post);
    }

    public function getUuid(): string
    {
        return $this->Persistence_Object_Identifier;
    }
}
