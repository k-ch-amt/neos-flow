<?php

declare(strict_types=1);

namespace Acme\Blog\Domain\Model;

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Flow\Entity
 */
class Post
{
    /**
     * @Flow\Validate(type="NotEmpty")
     * @ORM\ManyToOne(inversedBy="posts", targetEntity="Acme\Blog\Domain\Model\Blog")
     * @var Blog
     */
    protected $blog;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $subject;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @var \DateTime
     */
    protected $date;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $author;

    /**
     * @Flow\Validate(type="NotEmpty")
     * @ORM\Column(type="text")
     * @var string
     */
    protected $content;

    /**
     * @ORM\OneToMany(targetEntity="Acme\Blog\Domain\Model\Comment")
     * @var Collection<Comment>
     */
    protected $comments;

    /**
     * @ORM\ManyToMany(targetEntity="Acme\Blog\Domain\Model\Tag")
     * @var Collection<Tag>
     */
    protected $tags;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getBlog(): Blog
    {
        return $this->blog;
    }

    public function setBlog(Blog $blog): void
    {
        $this->blog = $blog;
        $this->blog->addPost($this);
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): void
    {
        $this->comments->add($comment);
    }

    public function deleteComment(Comment $comment): void
    {
        $this->comments->remove($comment);
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): void
    {
        $this->tags->add($tag);
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->remove($tag);
    }

    /**
     * @param Collection $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @param Collection $comments
     */
    public function setComments($comments): void
    {
        $this->comments = $comments;
    }

    public function getUuid(): string
    {
        return $this->Persistence_Object_Identifier;
    }
}
