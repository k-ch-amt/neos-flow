<?php

declare(strict_types=1);

namespace Acme\Blog\Domain\Repository;

use Acme\Blog\Domain\Model\Blog;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class BlogRepository extends Repository
{
    /**
     * @return Blog|null
     */
    public function findActive(): ?Blog
    {
        $query = $this->createQuery();

        return $query->execute()->getFirst();
    }
}
