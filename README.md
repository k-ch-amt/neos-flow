**NEOS FLOW - EXAMPLE**

**Docker Installation**
1. Install docker https://docs.docker.com/engine/install
2. Install docker compose https://docs.docker.com/compose/install/

**Setting up dev environment**
1. Go to docker/ directory.
2. Copy .env.dist to .env (in `docker` and `app` directory). 
3. Make sure that the environment variables in both .env files resemble parameters in Settings.yml.
4. Spin-up the environment `docker-compose up`.
5. Run `docker-compose exec web composer install`. If composer ask about token for the flow package then just use token from file `auth.json` in `app` directory.
6. Run migrations `docker-compose exec web ./flow doctrine:migrate` to update database.
7. By default, project should be accessible via http://neos.docker.localhost.

**Commands**  
`docker-compose up` - Start project  
`docker-compose down` - Remove project  
`docker-compose build` - Rebuild project if any changes in Docker configuration occurred


**REST**

Project contains experimental package `flowpack/rest-api` that can be used to generate REST endpoints for the resources.

Example for Post resource:

Endpoint: http://neos.docker.localhost/api/PostRest/create

Method: POST

JSON body:
```
{
    "resource": {
        "subject": "Test",
        "author": "John Doe",
        "blog": "22bcbcc6-d0f9-4c06-bf69-52f4124aaed9",
        "content": "Test",
        "tags": [
            {
                "name": "Test"
            },
            {
                "name": "Test"
            }
        ],
        "comments": [
            {
                "content": "123",
                "author": "Doe",
                "emailAddress": "doe@doe.doe"
            },
            {
                "content": "123",
                "author": "Dexter",
                "emailAddress": "doe@doe.doe"
            }
        ]
    }
}
```
**Standard actions**

Project contains also standard actions.
Example - show list of available blogs:`/blog/index`

**Available routes**
![Routes](routes.png)
